package ua.levelup.fadeew;

import java.io.File;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class Dispatcher {
    private final Config config;
    private final Handler handler;
    private final ViewResolver viewResolver;
    private final Result result;
    private final ConsoleView consoleView;


    public Dispatcher(String[] args) throws ConfigException {
        this.config = new Config(args);
        this.handler = new Handler();
        this.viewResolver = new ViewResolver();
        this.result = new Result();
        this.consoleView = new ConsoleView();
    }

    public void dispatch() throws DispatchException {
        if (this.config.getDestination().isFile()) {
            this.result.addMatches(this.config.getDestination(),
                    this.handler.handle(this.config.getDestination(),
                    this.config.getText4Search()));
        } else if (this.config.getDestination().isDirectory()) {
            for (File files : this.config.getDestination().listFiles()) {
                if (files.isFile()) {
                    List<ResultItem> results = this.handler.handle(files,
                            this.config.getText4Search());
                    if (results.size() != 0) {
                        this.result.addMatches(files, results);
                    }
                }
            }
        }
        if (!this.result.isEmpty()) {
            printResult(this.config.getText4Search());
        } else {
            throw new DispatchException("Не найдено ни одного совпадения");
        }
    }

    private void printResult(String string) {
        File key;
        List<ResultItem> value;
        int matches = 0;
        for (List<ResultItem> list : this.result.getMatches().values()) {
            matches += list.size();
        }
        if (this.config.getOutputFile() == null) {
            System.out.println("=========================");
            System.out.println("Smart File Handler CLI");
            System.out.println("=========================");
            System.out.println("Found " + matches + " matches for wanted text: \"" + string + "\"");
            for (Map.Entry<File, List<ResultItem>> pars : this.result.getMatches().entrySet()) {
                key = pars.getKey();
                value = pars.getValue();
                System.out.println("- Source: " + key.getAbsolutePath());
                System.out.println("Total matches: " + value.size());
                List<String> res = this.viewResolver.resolve(value);
                for (int i = 0; i < res.size(); i++) {
                    String show = res.get(i);
                    this.consoleView.print(show);
                }
            }
            System.out.println("=========================");
        } else {
            try {
                FileWriter writer = new FileWriter(this.config.getOutputFile());
                writer.write("Found " + matches + " matches for wanted text: \"" + string + "\"\n");
                for (Map.Entry<File, List<ResultItem>> pars : this.result.getMatches().entrySet()) {
                    key = pars.getKey();
                    value = pars.getValue();
                    List<String> res = this.viewResolver.resolve(value);
                    writer.write("- Source: " + key.getAbsolutePath() + "\n");
                    writer.write("Total matches: " + value.size() + "\n");
                    for (int i = 0; i < res.size(); i++) {
                        writer.write(" -> " + res.get(i) + "\n");
                    }
                }
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Ошибка при записи в файл");
            }
        }
    }
}

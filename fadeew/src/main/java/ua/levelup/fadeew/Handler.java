package ua.levelup.fadeew;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Handler {
    private final List<Parser> parsers;

    public Handler() {
        this.parsers = new ArrayList<>();
        this.parsers.add(new TxtParser());
        this.parsers.add(new XmlParser());
    }

    public List<ResultItem> handle(File file, String text) {
        List<ResultItem> results = new ArrayList<>();
        for (Parser parser : this.parsers) {
            if (parser.isSupportFile(file.getPath())) {
                results.addAll(parser.parse(file, text));
            }
        }
        return results;
    }
}

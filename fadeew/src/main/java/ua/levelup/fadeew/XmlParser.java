package ua.levelup.fadeew;

import java.io.File;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;





public class XmlParser extends AbstractParser {
    private final List<ResultItem> resultItems;

    public XmlParser() {
        this.resultItems = new ArrayList<>();
    }

    @Override
    public List<ResultItem> parse(File file, String string) {
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.parse(file);
            if (document.hasChildNodes()) {
                printInfoAboutChilds(document.getChildNodes(), string);
            } else if (document.getNodeValue().contains(string)) {
                this.resultItems.add(new XmlResultItem(document.getNodeName()
                        + document.getNodeValue()));
            }
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return resultItems;
    }

    public void printInfoAboutChilds(NodeList list, String s) {
        for (int i = 0; i < list.getLength(); i++) {
            Node node = list.item(i);
            if (node.hasChildNodes()) {
                printInfoAboutChilds(node.getChildNodes(), s);
            } else if (node.getNodeValue().contains(s)) {
                String path = " ->" + node.getNodeValue();
                while (node != null) {
                    path = node.getNodeName() + "/" + path;
                    node = node.getParentNode();
                }
                this.resultItems.add(new XmlResultItem(path));
            }
        }
    }

    @Override
    public boolean isSupportFile(String destination) {
        String expansion = destination.substring(destination.lastIndexOf("."));
        if (expansion.equals(".xml") || expansion.equals(".html")) {
            return true;
        } else {
            return false;
        }
    }
}

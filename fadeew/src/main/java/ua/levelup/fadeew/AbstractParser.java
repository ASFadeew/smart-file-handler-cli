package ua.levelup.fadeew;

import java.util.List;

public abstract class AbstractParser implements Parser {
    List<String> supportedExtensions;

    @Override
    public List<String> getSupportedExtensions() {
        return this.supportedExtensions;
    }
}

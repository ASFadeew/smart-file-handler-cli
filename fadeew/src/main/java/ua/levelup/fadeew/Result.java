package ua.levelup.fadeew;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Result {
    private Map<File, List<ResultItem>> matches = new HashMap<>();

    public void addMatches(File file, List<ResultItem> resultItemList) {
        this.matches.put(file, resultItemList);
    }

    public Map<File, List<ResultItem>> getMatches() {
        return matches;
    }

    public boolean isEmpty() {
        return this.matches.isEmpty();
    }
}

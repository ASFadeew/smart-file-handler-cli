package ua.levelup.fadeew;

import java.util.ArrayList;
import java.util.List;

public class ViewResolver {
    public List<String> resolve(List<ResultItem> value) {
        List<String> strings = new ArrayList<>();
        for (int i = 0; i < value.size(); i++) {
            strings.add(value.get(i).toString());
        }
        return strings;
    }
}

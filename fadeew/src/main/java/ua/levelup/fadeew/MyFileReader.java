package ua.levelup.fadeew;

import java.io.File;

import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;

public class MyFileReader {

    public List<String> lineReader(File file) throws IOException {
        List<String> strings = new ArrayList<>();
        String line;
        LineNumberReader reader = new LineNumberReader(new FileReader(file));
        while ((line = reader.readLine()) != null) {
            strings.add(line);
        }
        return strings;
    }
}

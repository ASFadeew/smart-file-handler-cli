package ua.levelup.fadeew;

public class DispatchException extends Throwable {

    public DispatchException() {
    }

    public DispatchException(String string) {
        System.out.println(string);
    }

    public DispatchException(String string, Throwable throwable) {
    }

    public DispatchException(Throwable throwable) {
    }

    public DispatchException(String str, Throwable throwable, boolean boolean1, boolean boolean2) {
    }
}

package ua.levelup.fadeew;

public class XmlResultItem implements ResultItem {
    private String node;

    public XmlResultItem(String node) {
        this.node = node;
    }

    @Override
    public String toString() {
        return " -> " + this.node;
    }
}

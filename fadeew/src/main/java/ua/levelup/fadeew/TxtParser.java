package ua.levelup.fadeew;

import java.io.File;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TxtParser extends AbstractParser {
    private MyFileReader fileReader;

    public TxtParser() {
        fileReader = new MyFileReader();
    }

    @Override
    public List<ResultItem> parse(File fileForSearch, String text) {
        List<ResultItem> resultItems = new ArrayList<>();
        try {
            List<String> strings = fileReader.lineReader(fileForSearch);
            String line;
            for (int i = 0; i < strings.size(); i++) {
                line = strings.get(i);
                if (line.contains(text)) {
                    resultItems.add(new SimpleResultItem(i, line));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Ошибка при считывании файла");
        }
        return resultItems;
    }

    @Override
    public boolean isSupportFile(String destination) {
        String expansion = destination.substring(destination.lastIndexOf("."));
        if (expansion.equals(".txt")) {
            return true;
        } else {
            return false;
        }
    }
}

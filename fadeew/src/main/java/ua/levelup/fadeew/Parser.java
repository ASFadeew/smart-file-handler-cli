package ua.levelup.fadeew;

import java.io.File;
import java.util.List;

public interface Parser {
    List<ResultItem> parse(File file, String string);

    boolean isSupportFile(String string);

    List<String> getSupportedExtensions();
}
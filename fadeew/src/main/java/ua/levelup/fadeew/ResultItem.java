package ua.levelup.fadeew;

public interface ResultItem {

    @Override
    String toString();
}

package ua.levelup.fadeew;

import java.io.File;

public class Config {
    private String outputFileExtension;
    private boolean printFullPath;
    private File outputFile;
    private String text4Search;
    private File destination;
    private boolean searchSubfolders;

    public Config(String[] args) throws ConfigException {
        parseArgs(args);
    }

    public String getOutputFileExtension() {
        return this.outputFileExtension;
    }

    public boolean isPrintFullPath() {
        return this.printFullPath;
    }

    public File getOutputFile() {
        return this.outputFile;
    }

    public String getText4Search() {
        return this.text4Search;
    }

    public File getDestination() {
        return this.destination;
    }

    public boolean isSearchSubfolders() {
        return this.searchSubfolders;
    }

    private void parseArgs(String[] args) throws ConfigException {
        if (args.length < 2) {
            throw new ConfigException("Недостаточно аргументов");
        }
        File filePath = new File(args[args.length - 1]);
        if (args[args.length - 1] != null && !args[args.length - 1].equals("")) {
            if (!filePath.exists()) {
                throw new ConfigException("Файл не найден");
            }
            this.destination = filePath;
        } else {
            throw new ConfigException("Значение аргумента \"Путь к файлу/папке\" - null");
        }
        if (args[args.length - 2] != null && !args[args.length - 2].equals("")) {
            this.text4Search = args[args.length - 2];
        } else {
            throw new ConfigException("Значение аргумента \"Текст для поиска\" - null");
        }
        for (int i = 0; i <= args.length - 3; i++) {
            switch (args[i]) {
              case "-o":
                  if (!args[i + 1].equals("-s") && !args[i + 1].equals("-p")
                          && !args[i + 1].equals("-rf")) {
                      this.outputFile = new File(args[i + 1]);
                      i++;
                  }
                  break;
              case "-s":
                  if (!args[i + 1].equals("-o") && !args[i + 1].equals("-p")
                          && !args[i + 1].equals("-rf")) {
                      if (this.outputFile != null) {
                          this.outputFileExtension = args[i + 1];
                          StringBuilder sb = new StringBuilder();
                          String oldFile = this.outputFile.getPath();
                          String pathNewFile = oldFile.substring(0, oldFile.lastIndexOf('.'));
                          sb.append(pathNewFile).append(".").append(this.outputFileExtension);
                          this.outputFile = new File(sb.toString());
                      }
                      i++;
                  }
                  break;
              case "-p":
                  if (this.outputFileExtension != null) {
                      this.printFullPath = true;
                  }
                  break;
              case "-rf":
                  this.searchSubfolders = true;
                  break;
              default:
                  if (args[i].charAt(0) != 'C' && args[i].charAt(0) != 'D') {
                      this.outputFileExtension = args[i];
                  } else {
                      this.outputFile = new File(args[i]);
                  }
                  break;
            }
        }
    }
}
